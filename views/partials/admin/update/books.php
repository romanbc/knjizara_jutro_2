    <p class="text-primary">Izmena knjige</p>
    <div class="form-group">
        <input type="text" class="form-control name-100" name="tbName" value="<?= $name ?>"
            placeholder="Unesite naziv" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="tbPrice" value="<?= $price ?>"
            placeholder="Unesite cenu" />
    </div>
    <div class="form-group">
        <textarea class="form-control" placeholder="Unesite opis" name="taDesc" maxlength="1000"><?= $desc?></textarea>
    </div>
    <form>
    <div class="form-group">
        <input  type="checkbox" name="chbChangeImg" value="1" id="chbChangeImg"/> <label for="chbChangeImg">Promeni sliku</label>
        <input type="file" class="form-control-file" id="fImg" name="fImg">
        <input type="hidden" name="imgId" value="<?= $imgId?>" />
    </div>

    <div class="form-group">
        <input type="text" class="form-control" name="tbImgName" value="<?= $imgName; ?>"
            placeholder="Unesite ime slike" />
        <small class="form-text text-muted">
            Dozvoljena slova engleskog alfabeta, cifre i donja crta _
        </small>
    </div>

    <div class="form-group">
        <input type="text" class="form-control name-100" name="tbAlt" value="<?= $img->alt; ?>"
            placeholder="Unesite opis slike" />
    </div>
    <div class="form-row form-group">
        <div class="col-md-6">
            <input type="number" class="form-control" name="tbYear" min="1960" max="<?= date("Y"); ?>" 
               value="<?= $year ?>" placeholder="Godina izdanja" required/>
        </div>
        <div class="col-md-6">
            <input type="number" class="form-control" name="tbPages" min="1" max="99999"
            value="<?= $pages ?>" placeholder="Broj strana" required/>
        </div>
    </div>

    <div class="form-group">
      <label for="ddlPublisher">Izaberite izdavača </label>
      <select class="form-control" id="ddlPublisher" name="ddlPublisher">
        <?php foreach($allP as $p) : ?>
            <option value="<?= $p->id?>"  <?=  $publisher == $p->id ? " selected" : ""; ?>   > <?= $p->naziv ?>  </option>
        <?php  endforeach; ?>
      </select>

      <label for="ddlGenre">Izaberite žanr knjige (može ih biti više)</label>
      <select multiple class="form-control" id="ddlGenre" name="ddlGenre[]">
        <?php  foreach($allG as $g) :  ?>
            <option value="<?=$g->id?>" <?php foreach($genres as $g1) { if($g1->id_zanr == $g->id) { echo (" selected") ;}  } ?> > <?= $g->naziv ?>  </option>
        <?php   endforeach; ?>
      </select>

      <label for="ddlAuthor">Izaberite autora knjige (može ih biti više)</label>
      <select multiple class="form-control" id="ddlAuthor" name="ddlAuthor[]">
        <?php foreach($allA as $a) : ?>
            <option value="<?= $a->id?>"  <?php foreach($authors as $a1) { if($a1->id_autor == $a->id) { echo (" selected") ;}  } ?> > <?= $a->autor ?>  </option>
        <?php endforeach; ?>
      </select>
    </div>


