    <p class="text-primary">Izmena korisnika</p>
    <div class="form-group">
    <input type="text" class="form-control person-name"  name="tbFName" value="<?= $fName?>"
        placeholder="Unesite ime" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control person-name"  name="tbLName" value="<?= $lName?>"
            placeholder="Unesite prezime" />
    </div>
    <div class="form-group">
        <input type="text" class="form-control"  name="tbMail" value="<?= $mail ?>"
            placeholder="Unesite e-mail" />
    </div>
    <div class="form-group">
        <input type="password" class="form-control"  name="tbPass" value="abcd4%"
            placeholder="Unesite lozinku" />
    <small class="form-text text-muted">
        Najmanje 6 znakova, makar jedno slovo, cifra i
        specijalni znak
    </small>
    </div>
    <div class="form-group">
        <label class="checkbox-inline mr-2"><input type="checkbox" value="1" name="chbChangePass" />Promeni lozinku
        <label class="checkbox-inline mr-2"><input type="checkbox" value="1" name="chbActive" <?= $active ? "checked" : ""; ?>>Aktiviran</label> 
        <label class="radio-inline"><input type="radio" value="1" name="rbRole"<?= $role == 1 ? " checked" : "";?> >Korisnik</label>
        <label class="radio-inline"><input type="radio" value="2" name="rbRole" <?= $role == 2 ? " checked" : "";?> >Administrator</label>
    </div>
