<?php             
            
    $html = '<table class="table table-responsive-lg text-center">  <thead class="text-info"><tr><th>Id</th>';

    foreach($ths as $th) {
        $html .= '<th>' . $th . '</th>'; 
    }
    $html .= "<th>Izmeni</th> <th>Ukloni</th></tr></thead><tbody>";
    


    foreach($trs as $tr) {

                    
        $html .= '<tr>';
        foreach($tr as $td) {
            $html .= '<td class="align-middle">' . $td . '</td>';
        }
        
        $id = $tr -> id;

        $html .= '
        <td class="align-middle">
            <a class="btn btn-outline-info my-2" href="' . BASE_PATH . '/admin/' . $table . '/update/' . $id . '">Izmeni</a>
        </td>
        <td class="align-middle text-danger">
            <form name="formDelete" method="POST" action="' . BASE_PATH . '/models/admin/delete.php" >
                <button type="submit" name="btnSubmit" class="btn btn-sm btn-outline-danger">
                    <i class="icon-remove"></i>
                </button>
                <input type="hidden" name="id" value="' . $id . '" />
                <input type="hidden" name="what" value="' . $table . '" />
            </form>
        </td></tr>';        
    }


    $html .= '</tbody></table>';
                
            

?>