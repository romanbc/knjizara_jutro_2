<form name="formAdd" method="POST" action="<?= BASE_PATH ?>/models/admin/add.php" <?= $table == "books" ? ' enctype="multipart/form-data" ' : "" ?>>
<?php 

    require_once "add/{$table}.php";
?>
    <input type="hidden" name="what" value="<?= $table ?>" />
    <input type="reset" class="btn btn-outline-danger" value="Poništi" name="btnReset" />
    <input type="submit" class="btn btn-outline-primary" value="Dodaj" name="btnSubmit" />
</form>
