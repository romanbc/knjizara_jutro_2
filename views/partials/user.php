<tr>
    <td class="align-middle">
        <img alt="<?=$r->alt?>" src="<?= substr($r->src, 4) ?>" width="60em">
    </td>
    <td class="align-middle text-primary">
        <?= $r->naziv?>
    </td>
    <td class="align-middle"> <?=$r->cena ?> RSD</td>
    <td class="align-middle" >
        <?=$r->kolicina ?>
    </td>
    <td class="align-middle text-primary"><?=$r->ukupno ?> RSD</td>
    <td class="align-middle"><?= date('Y-m-d h:i:s', $r->vreme_kupovine) ?></td>
</tr>