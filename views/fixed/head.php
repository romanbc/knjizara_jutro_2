<!DOCTYPE html>
<html lang="sr">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?= $page_metadata['title'] ?></title>
    <meta name="description" content="<?= $page_metadata['description'] ?>" />
    <meta name="keywords" content="<?= $page_metadata['keywords']?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="robots" content="index, follow" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="<?= BASE_PATH?>/assets/css/styleUser.css" />
    <link rel="stylesheet" href="<?= BASE_PATH?>/assets/css/style.min.css" />
</head>

<body>

