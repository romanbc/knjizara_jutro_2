<?php 

$menu_items = getMenuItems();

foreach ($menu_items as $menu_item) :
?>
    <li class="nav-item <?= ($page) == $menu_item -> href || ($menu_item -> href == "./" && $page == "main") ? "active" : ""?>"><a class="nav-link font-weight-bold" href="
    <?= BASE_PATH . "/" . $menu_item -> href ?>"><?= $menu_item -> text ?></a></li>
<?php endforeach; ?>   