<?php 
    if (isset($_SESSION['user'])) {
        $logged = true;
        if($_SESSION['user']->naziv == "korisnik") {
            $user = true;
        } 
        if($_SESSION['user']->naziv == "admin") {
            $admin = true;
        }
    }
?>

<div class="container-fluid bg-primary">
    <div class="container ">
        <div class="row position-relative">
            <nav class="col-11 navbar navbar-expand-md navbar-dark">
                
                <a class="navbar-brand" href="<?= BASE_PATH ?>/"><img src="<?= BASE_PATH?>/assets/images/logo.png" class="img-fluid"
                        id="logo" alt="logo" /></a>
                <button class="navbar-toggler bg-white" type="button" data-toggle="collapse"
                    data-target="#navHeader" aria-label="navHeader toggler">
                    <span class="icon-bars text-primary"></span>
                </button>
                <div class="collapse navbar-collapse text-right" id="navHeader">
                    <div class="mr-auto">
                        <h1  id="heading">
                            <a href="<?= BASE_PATH ?>/" class="nav-link font-weight-bold">Knjižara Jutro
                            </a>
                        </h1>
                    </div>
                    <ul class="navbar-nav mr-3">
                        <?php require "nav.php";
                        if(isset($logged)) :
                            if(isset($user)) :
                        ?>
                        <li class="nav-item <?= $page == "user" ? "active" : "" ?>">
                        <a class="nav-link font-weight-bold" href="<?= BASE_PATH ?>/user"> <span class="icon-user "></span></a></li>
                            <?php endif; if(isset($admin)): ?>
                        <li class="nav-item <?= $page == "admin" ? "active" : "" ?>">
                        <a class="nav-link font-weight-bold" href="<?= BASE_PATH ?>/admin"> <span class="icon-user-tie"></span></a></li>
                            <?php endif; ?>
                        <li class="nav-item ">
                        <a class="nav-link font-weight-bold" href="<?= BASE_PATH ?>/models/account/logout.php"> <span class="icon-exit "></span></a></li>
                            <?php else: ?>
                        <li class="nav-item <?= $page == "account" ? "active" : "" ?>">
                        <a class="nav-link font-weight-bold" href="<?= BASE_PATH ?>/account"> <span class="icon-enter "></span></a></li>
                            <?php endif; ?>
                    </ul>
                </div>
            </nav>
            <?php if(empty($admin)) : ?>
            <div class="col-1 text-right position-absolute cart-icon">
                <a href="cart">
                    <span class="icon-shopping-cart"></span>
                    <span id="cart-count">0</span>
                </a>
            </div>
            <?php   endif; ?>
        </div>
    </div>
</div>
