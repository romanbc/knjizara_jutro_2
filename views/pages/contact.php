<?php 
    if(isset($_SESSION['poslata'])) {
        echo "<script>alert(\"" . $_SESSION['poslata'] . "\")</script>";   
        unset($_SESSION['poslata']);
    }

?>

<div class="min-h container my-3">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h2 class="text-center text-primary my-4">Kontaktirajte nas</h2>
            <form name="formConact" action="models/contactAdmin.php" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control name-100" name="tbName" placeholder="Unesite Vaše ime i prezime" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="tbMail" placeholder="Unesite Vašu email adresu" />
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Vaša poruka" name="taBody" maxlength="1000"></textarea>
                </div>

                <input type="submit" class="btn btn-outline-primary" value="Pošalji" name="btnSubmit" />
            </form>


        </div>
    </div>
</div>