<?php 
    
    
    $name = $_SESSION['user']->ime;
    $id = $_SESSION['user']->id;

    require_once "models/user.php";
?>

<div class="container my-2 my-lg-4  min-h   ">
    <div class="row pt-2" id="main">
        <div class="col-md-10 offset-md-1 text-center">
         <h2 class="text-primary" >Zdravo <?= $name?></h2>
         <div id="current" class="m-4"></div>

        </div>
    </div>
    <div class="text-center">
        <h3 class="text-primary">Istorija kupovine </h3>
        <?php  if(isset($rows) && $rows):  ?>

        <table class="table table-responsive-lg text-center">
            <thead class="text-info">
                <tr>
                    <th></th>
                    <th>Knjiga</th>
                    <th>Cena</th>
                    <th>Količina</th>
                    <th>Ukupno</th>
                    <th>Vreme </th>
                </tr>
            </thead>
        <tbody>
        <?php 
            foreach($rows as $r) {
                require  "views/partials/user.php";
            }
            ?>
            

    </tbody></table>
<?php else: echo "<p>Niste nikad ništa kupili kod nas</p>";  endif; ?>
    </div>
</div>