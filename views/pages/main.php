<?php 





  
?>
    


<div class="container my-2 my-lg-4">
    <div class="row min-h" id="main">


        <div class="col-md-3">
            <nav class="navbar-expand-md p-1">
                <button class="navbar-toggler w-100 mb-1 text-primary font-weight-bold" type="button"
                    data-toggle="collapse" data-target="#navFilter">Prilagodi</button>
                <div class="list-group list-group-flush collapse navbar-collapse" id="navFilter">
                    <form method="GET" action="" id="formFS">
                    <div>
                        <div class="border-bottom">
                            <button class="list-group-item w-100 text-left text-primary btn-outline-light" type="button"
                                data-toggle="collapse" data-target="#card-filter-genre">
                                Žanr
                            </button>
                                <div class="collapse" id="card-filter-genre">
                                    <div class="card card-body border-0 mt-1 py-1">
                                        <select name="ddlGenre" class="form-control">
                                 
                                        </select>
                                    </div>
                                </div>
                        </div>

                        <div class="border-bottom">
                            <button type="button"
                                class="list-group-item w-100 text-left text-primary btn-outline-light "
                                data-toggle="collapse" data-target="#card-filter-publisher">
                                Izdavač
                            </button>
                            <div class="collapse" id="card-filter-publisher">
                                <div class="card card-body border-0 mt-1 py-1">
                                    <div class="form-group" id="publishers">

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="border-bottom">
                            <button class="list-group-item w-100 text-left text-primary btn-outline-light"
                                data-toggle="collapse" data-target="#card-sort" type="button">
                                Sortiraj po
                            </button>
                                <div class="collapse" id="card-sort">
                                    <div class="card card-body border-0 mt-1 py-1">
                                    <select name="ddlSort" class="form-control">
                                        <option value="asc-naziv" >Nazivu A-Z</option>
                                        <option value="desc-naziv">Nazivu Z-A</option>
                                        <option value="asc-cena" >Najjeftiniji </option>
                                        <option value="desc-cena">Najskuplji</option>
                                     </select>
                                    </div>
                                </div>
                        </div>
                        <div class="mt-3">
                                <button type="reset" id="resetFilters" class="btn btn-outline-danger form-control m-1">
                                    Poništi
                                </button>
                                <button type="submit"  class="btn btn-outline-success form-control m-1">
                                    Primeni
                                </button>
                        </div>
                    </div>
                </form>
                </div>
            </nav>
        </div>

        <div class="col-md-9">
            <div  class="row" id="books">            </div>

            <ul class="nav nav-pills" id="pagination">             </ul>

        </div>
    </div>
</div>


<div class="modal fade" id="bookDetails" tabindex="-1">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="d-flex justify-content-between">
                <div></div>
                <button type="button" class="close p-3 p-md-2" data-dismiss="modal">
                    <span class="icon-close text-danger "></span>
                </button>
            </div>
            <div class="modal-body pt-0"></div>
        </div>
    </div>
</div>