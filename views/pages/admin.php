<?php
if (isset($_SESSION['errAdd'])) {
    echo "<script>alert(\"" . $_SESSION['errAdd'] . "\")</script>";
    unset($_SESSION['errAdd']);
}
if (isset($_SESSION['sucAdd'])) {
    echo "<script>alert(\"" . $_SESSION['sucAdd'] . "\")</script>";
    unset($_SESSION['sucAdd']);
}
$name = $_SESSION['user']->ime;
$tables = [
    ["books", "Knjige"],
    ["genres", "Žanrovi"],
    ["authors", "Autori"],
    ["publishers", "Izdavači"],
    ["users", "Korisnici"],
];

require_once "models/admin/functions.php";

$table = retrieveGetParam("table");
$action = retrieveGetParam("action");
$id = retrieveGetParam("id");

$showAll = $table && !$action ? true : false;

 //var_dump($table, $action, $id, $showAll);
 //var_dump($_GET);
// var_dump($_SERVER);

?>

<div class="container my-2 my-lg-4 min-h">
    <div class="row pt-2" id="main">
        <div class="col-md-10 offset-md-1 text-center text-primary">



        <?php if (!$table): ?><h2 >Zdravo <?=$name?></h2> <?php require_once "models/admin/log_stats.php";  endif;?>

            <ul class="nav justify-content-center py-4">
            <?php foreach ($tables as $t): ?>
                <li class="nav-item m-1"><a href="<?=BASE_PATH . "/admin/" . $t[0]?>" class="btn btn-outline-primary <?=$table == $t[0] ? "active" : ""?>" ><?=$t[1]?></a></li>
            <?php endforeach;?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <p class="text-info py-1">
                <?php if ($table): echo (getTableManual($table, $action));else: ?>
                    Izaberite tabelu iz baze čije podatke želite menjati , možete dodavati, menjati i brisati zapise.
                <?php endif;?>
            </p>
<?php


if ($showAll) {
    echo ('<a class="btn btn-outline-primary my-2" href="' . BASE_PATH . '/admin/' . $table . '/add">Dodaj</a>');
    echo populateTable($table);
} else if ($action == "add") {
    if($table == "books") {
        require_once "models/admin/add/retrieve/books.php";
    }
    // require_once "views/partials/admin/add/{$table}.php";
    require_once "views/partials/admin/add.php";
} else if ($action == "update") {    
    require_once "models/admin/update/retrieve/{$table}.php";
    require_once "views/partials/admin/update.php";
}

?>
        </div>
    </div>








</div>