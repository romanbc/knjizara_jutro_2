<?php 
    session_start();
    if(isset($_GET['t']) && isset($_GET['m'])) {
        require_once ("../../config/connection.php");
        $time_reg = $_GET['t'];
        $mail = $_GET['m'];
        $q1 = "UPDATE korisnik SET aktivan = 1 WHERE vreme_registracije = :time_reg AND email = :mail";
        $stmt = $conn->prepare($q1);
        $stmt->bindParam(":time_reg", $time_reg);
        $stmt->bindParam(":mail", $mail);
        $result = $stmt->execute();
        if($result) {
            $_SESSION['uspeh'] = "Uspesna verifikacija, sada Vam je omoguceno logovanje";
            header("Location: ../../account");
        } else {
            $_SESSION['greske'] = "Nije uspela verifikacija";
            header("Location: ../../account");
        }
    } else {
        header("Location: ../../account");
    }
?>