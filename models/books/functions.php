<?php

define("BOOK_OFFSET", 6); // Koliko se prikazuje po stranici. Da ne bismo u slucaju promene ovog broja morali na dosta mesta da menjamo, stavljamo u konstantu.

function get_books($limit = 0, $genreId = 0, $publisherIds = 0, $sortBy = 0){
    global $conn;
    try{
        

        $query = "select k.id, k.naziv, k.cena, s.src, s.alt, GROUP_CONCAT(CONcat(a.ime, ' ', a.prezime) SEPARATOR ', ') as autor from knjiga k inner join slika s on k.id_slika = s.id inner join knjiga_autor ka on k.id = ka.id_knjiga inner join autor a on ka.id_autor = a.id  "  . ($genreId ?  " inner join knjiga_zanr kz on kz.id_knjiga = k.id " : " " ) . ($publisherIds ? " inner join izdavac i on k.id_izdavac = i.id" : "") .  "  WHERE "  . (  $genreId ? " kz.id_zanr = $genreId  " : " 1 " ) .  " AND " . ($publisherIds ? "k.id_izdavac in($publisherIds)" : " 1 ") . "GROUP BY k.id"  . ($sortBy ? " ORDER BY k." . explode("-", $sortBy)[1] . "  " .  explode("-", $sortBy)[0] : "" ) . " LIMIT :limit, :offset "    ;
       


        $select = $conn -> prepare($query);



        $limit = ((int) $limit) * BOOK_OFFSET;


        $select->bindParam(":limit", $limit, PDO::PARAM_INT); 

        $offset = BOOK_OFFSET;
        $select->bindParam(":offset", $offset, PDO::PARAM_INT);

        $select->execute(); 

        $books = $select->fetchAll();

       
        return $books;
    }
    catch(PDOException $ex){
        zabeleziGresku($ex -> getMessage());

        return null;
    }
}



function get_book($id) {
    global $conn;

    $query = "
    SELECT
        k.id,
        k.naziv,
        k.cena,
        k.opis,
        k.godina_izdanja,
        k.strana,
        i.naziv as izdavac,
        s.src,
        s.alt,
        GROUP_CONCAT(
            CONCAT(a.ime, ' ', a.prezime) SEPARATOR ', '
        ) AS autor,
        (
        SELECT
            GROUP_CONCAT(z.naziv SEPARATOR ', ') AS zanr
        FROM
            zanr z
        INNER JOIN knjiga_zanr kz ON
            z.id = kz.id_zanr
        INNER JOIN knjiga k1 ON
            k1.id = kz.id_knjiga
        WHERE
            k1.id = ?
        GROUP BY
            k1.id
    ) AS zanr
    FROM
        knjiga k
    INNER JOIN slika s ON
        k.id_slika = s.id
    INNER JOIN knjiga_autor ka ON
        k.id = ka.id_knjiga
    INNER JOIN autor a ON
        ka.id_autor = a.id
    INNER JOIN izdavac i ON
        k.id_izdavac = i.id
    WHERE
        k.id = $id
    GROUP BY
        k.id
    ";


    $select = $conn -> prepare($query);

    $select->execute([$id]);
    
    $book = $select -> fetch();
    return $book;
}


function get_num_of_books(){
    return executeQueryOneRow("SELECT COUNT(*) AS num_of_books FROM knjiga");
}

function get_num_of_books_where($genreId, $publisherIds){
    // return executeQueryOneRow("SELECT COUNT(DISTINCT k.id) AS num_of_books FROM knjiga k INNER JOIN knjiga_zanr kz on kz.id_knjiga = k.id WHERE"   . (  $genreId ? " kz.id_zanr = $genreId  " : " 1 " ) );
    
    return executeQueryOneRow("SELECT COUNT(DISTINCT k.id) AS num_of_books FROM knjiga k" . ($genreId ?  " inner join knjiga_zanr kz on kz.id_knjiga = k.id " : " " ) . ($publisherIds ? " inner join izdavac i on k.id_izdavac = i.id" : "") .  "  WHERE "  . (  $genreId ? " kz.id_zanr = $genreId  " : " 1 " ) .
        " AND " . ($publisherIds ? "k.id_izdavac in($publisherIds)" : " 1 ") );
    
}

function get_pagination_count(){
    $result = get_num_of_books();
    $num_of_books = $result->num_of_books;

    return ceil($num_of_books / BOOK_OFFSET);
}

function get_pagination_count_where($genreId, $publisherIds){
    $result = get_num_of_books_where($genreId, $publisherIds);
    $num_of_books = $result->num_of_books;

    return ceil($num_of_books / BOOK_OFFSET);
}


?>