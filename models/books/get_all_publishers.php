<?php

header("Content-Type: application/json");

require_once "../../config/connection.php";

$genreId = $_GET['genreId'];

$publishers = executeQuery("
SELECT i.id, i.naziv, COUNT(k.id) AS broj FROM izdavac i LEFT JOIN knjiga k ON i.id = k.id_izdavac " . 
($genreId ? " INNER JOIN knjiga_zanr kz ON k.id = kz.id_knjiga WHERE kz.id_zanr = $genreId AND " : " WHERE ")
.  "  k.id IS NOT NULL  GROUP BY i.id order by broj desc, i.naziv 

");




echo json_encode($publishers);

?>