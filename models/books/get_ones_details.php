<?php

header("Content-Type: application/json");

if(isset($_GET['id'])){
    require_once "../../config/connection.php";
    include "functions.php";

    $id = $_GET['id'];

    $book = get_book($id);

    echo json_encode($book);
} else {
    echo json_encode(["message"=> "Limit not passed."]);
    http_response_code(400); // Bad request
}






//$src = substr($b->src, 1);

?>
