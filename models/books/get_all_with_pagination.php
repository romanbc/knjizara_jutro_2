<?php

header("Content-Type: application/json");

if(isset($_GET['limit'])){
    require_once "../../config/connection.php";
    include "functions.php";

    $limit = $_GET['limit'];

    $genreId = $_GET['genreId'];
    
    $publisherIds = $_GET['publisherIds'];

    $sortBy = $_GET['sortBy'];


    $books = get_books($limit, $genreId, $publisherIds, $sortBy);
    // $num_of_pages = get_pagination_count();
    $num_of_pages = get_pagination_count_where($genreId, $publisherIds);

    echo json_encode([
        "books" => $books,
        "num_of_pages" => $num_of_pages
    ]);
} else {
    echo json_encode(["message"=> "Limit not passed."]);
    http_response_code(400); // Bad request
}
?>