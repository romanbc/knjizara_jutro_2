<?php

header("Content-Type: application/json");

if (isset($_POST['safe'])) {
    require_once "../../config/connection.php";

    $ids = $_POST['ids'];
    $ids = implode(", ", $ids);
    $books = executeQuery("select k.id, k.naziv, k.cena, s.src, s.alt from knjiga k inner join slika s on k.id_slika = s.id where k.id in ($ids)");

    echo json_encode($books);

}

?>