<?php



function ifNotRoleForbid($role)
{
    if (!isset($_SESSION['user']) || $_SESSION['user']->naziv != $role) {
        $_SESSION['forbidden'] = "Nemate pravo pristupa!";
        header("Location: ./");
        // header("Location: " . BASE_PATH);
        die();
    }
}

function getPageMetadata($page)
{
    $meta = [
        "admin" => ["title" => "Knjižara Jutro | Admin", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "account" => ["title" => "Knjižara Jutro | Nalog", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "author" => ["title" => "Knjižara Jutro | Autor", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "cart" => ["title" => "Knjižara Jutro | Korpa", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "contact" => ["title" => "Knjižara Jutro | Kontakt", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "user" => ["title" => "Knjižara Jutro | Korisnik", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "main" => ["title" => "Knjižara Jutro", "description" => "Knjižara Jutro, administratorski nalog", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro administrator, Knjižara Jutro upravljanje sadržajem"],
        "404" => ["title" => "Knjižara Jutro | 404", "description" => "Knjižara Jutro, stranica ne postoji 404", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro 404, stranica ne postoji"],
        "403" => ["title" => "Knjižara Jutro | 403", "description" => "Knjižara Jutro, zabranjen pristup 404", "keywords" => "online prodaja knjiga, Knjižara Jutro, Knjižara Jutro 403, zabranjen pristup"]
    ];
    return $meta[$page];
}

function getMenuItems() {
    return executeQuery("SELECT href, text FROM meni");
}

?>
