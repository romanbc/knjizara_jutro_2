<?php

$sucMessage = "";

if (isset($_POST['chbChangeImg'])) {

    $alt = $_POST['tbAlt'];
    $imgName = $_POST['tbImgName'];
    $imgId = $_POST['imgId'];
    $file = $_FILES['fImg'];

    $errs = checkUploadedImage($file);

    if (count($errs) == 0) { 
        
        $sucMessage .= " Slika ispunjava kriterijume";

        $tmpPath = $file['tmp_name'];
        $fileName = time() . "_" . $imgName . "." . pathinfo($file['name'], PATHINFO_EXTENSION);
        $newPath = "../../assets/images/books/" . $fileName;
        $moveSucc = move_uploaded_file($tmpPath, $newPath);
    
        if ($moveSucc) { 

            $sucMessage .= "\\n Slika je upload-ovana na server";
      
            $oldSrc  = executeQueryOneRow("SELECT src FROM slika WHERE id = $imgId") -> src;    

            
            $query = "UPDATE slika SET src =  :src, alt =  :alt WHERE id = $imgId";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(":alt", $alt);
            $stmt->bindParam(":src", $newPath);

            try {

                $result = $stmt->execute();

                if($result){
                    $sucMessage .= "\\n Putanja do nove slike i ostale informacije su upisane u bazu";


                    if(file_exists($oldSrc)){
                        unlink($oldSrc);
                        $sucMessage .= "\\n Obrisana  je stara slika sa servera ";
                    }
                }                    
            

            } catch(PDOException $ex) {
                $erMessage =  " Greska pri upisu nove slike u bazu \\n " . $ex -> getMessage();             
            }

        } else {
            $erMessage = "Neuspeh pri upload-ovanju nove slike na server";    
        }

    } else {


        $erMessage = "Slika ne zadovoljava kriterijume: ";
    
        foreach ($errs as $err) {
            $erMessage .= "\\n{$err}";
        }
    
    }
}

if(!isset($erMessage)) {

    $name = $_POST['tbName'];
    $desc = $_POST['taDesc'];
    $price = $_POST['tbPrice'];
    $year = $_POST['tbYear'];
    $pages = $_POST['tbPages'];
    $publisher = $_POST['ddlPublisher'];
    $genre = $_POST['ddlGenre'];
    $author = $_POST['ddlAuthor'];


    try {

        $conn->beginTransaction();

        $query = "UPDATE knjiga SET naziv = :name, opis = :desc, cena  = :price, godina_izdanja = :year, strana = :pages, id_izdavac = $publisher WHERE id = $id";

        $stmt = $conn->prepare($query);


        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":desc", $desc);
        $stmt->bindParam(":price", $price);
        $stmt->bindParam(":year", $year);
        $stmt->bindParam(":pages", $pages);
   
    
        $success = $stmt->execute();

        if($success) {




            $conn->query("DELETE FROM knjiga_zanr WHERE id_knjiga = $id");
            $conn->query("DELETE FROM knjiga_autor WHERE id_knjiga = $id");

            foreach($genre as $g) {
                $query = "INSERT INTO knjiga_zanr VALUES(null, $id, $g)";
                $conn->query($query);
            }

            foreach($author as $a) {
                $query = "INSERT INTO knjiga_autor VALUES(null, $id, $a)";
                $conn->query($query);
            }

            $conn->commit();

            $sucMessage .= "\\n Uspesno izmenjena knjiga";

            $_SESSION['sucAdd'] = $sucMessage;
            header("Location: ../../admin/" . $what);
            die();
           
        } 

    } catch (PDOException $e) {

        $conn->rollback();

        $erMessage =  " Greska pri izmeni knjige u  bazi \\n " . $ex -> getMessage();  


    }


}

if (isset($erMessage)) {

    zabeleziGresku($erMessage);
    
    $_SESSION['errAdd'] = $erMessage . "\\n Zbog nastanka greske, nova slika je obrisana sa servera i nista nije upisano u bazu, pokusajte ponovo";

    header("Location: ../../admin/" . $what);
    die();
}


?>