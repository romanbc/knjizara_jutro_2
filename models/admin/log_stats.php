<?php 

    $open = fopen(LOG_FAJL, "r");
    if($open) {
        $podaci = file(LOG_FAJL);
        fclose($open);

        $n = count($podaci);
        define("DANSEKUNDE", 24 * 60 * 60);
        $logLast24h = [];
        for($i = 0; $i < $n; $i++) {                 
            $red = explode("\t", $podaci[$i]);
            $t = $red[1];                  
            $pre24h = time() - DANSEKUNDE;
            $utT = strtotime($t);
            if($utT >= $pre24h) {
                $logLast24h[] = $red[0];
            }
        }
        // var_dump($logLast24h);

        $n = count($logLast24h);

        $logLast24hDistinct = [];

        for($i = 0; $i < $n; $i++) {
            if(!in_array($logLast24h[$i], $logLast24hDistinct )) {
                $logLast24hDistinct[] = $logLast24h[$i];
            }
        }

        // var_dump($logLast24hDistinct);

        $arrr = [];

        foreach($logLast24hDistinct as $k => $v) {
            // echo "$k : $v " ;
            $arrr[$v] = 0;
        }

        // var_dump($arrr);
                        
        foreach($arrr as $k => $v) {
            foreach($logLast24h as $log) {
                if($k == $log) {
                    $arrr[$k] += 1;
                }
            }
        }

        // var_dump($arrr);

        
        $ukupno = 0;
        foreach($arrr as $ar) {
            $ukupno += $ar;
        }

        // var_dump($ukupno);

        echo " <h5> Statistika pristupa stranama u poslednja 24 sata </h5> <p> Ukupan broj poseta: $ukupno </p> <ul class=\"list-group\"> ";

        
        foreach ($arrr as $k => $v) {
            echo " <li class='list-group-item' >  $k : " . round($v * 100 / $ukupno, 2 ) . "% : $v puta </li>";
        }

        echo "</ul>";

        
    }



?>