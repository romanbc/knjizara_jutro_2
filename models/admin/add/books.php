<?php

$alt = $_POST['tbAlt'];
$imgName = $_POST['tbImgName'];
$file = $_FILES['fImg'];

$errs = checkUploadedImage($file);

if (count($errs) == 0) {

    $sucMessage = " Slika ispunjava kriterijume";

    $tmpPath = $file['tmp_name'];
    $fileName = time() . "_" . $imgName . "." . pathinfo($file['name'], PATHINFO_EXTENSION);
    $newPath = "../../assets/images/books/" . $fileName;
    $moveSucc = move_uploaded_file($tmpPath, $newPath);

    if ($moveSucc) {
        
        $sucMessage .= "\\n Slika je upload-ovana na server";

        try {
            $conn->beginTransaction();

            $query = "INSERT INTO slika VALUES (null, ?, ?)";
            $stmt = $conn->prepare($query);
            $result = $stmt->execute([$newPath, $alt]);

            if ($result) {
                $sucMessage .= "\\n Putanja do slike i ostale informacije su upisane u bazu";

                $id_pic = $conn -> query("select id as last from slika where src = '$newPath'") -> fetch() ->last;

                $name = $_POST['tbName'];
                $desc = $_POST['taDesc'];
                $price = $_POST['tbPrice'];
                $year = $_POST['tbYear'];
                $pages = $_POST['tbPages'];
                $publisher = $_POST['ddlPublisher'];
                $genre = $_POST['ddlGenre'];
                $author = $_POST['ddlAuthor'];

                $query = "INSERT INTO knjiga VALUES(null, ?, ?, ?, ?, ?, $publisher, $id_pic) ";

                $stmt = $conn->prepare($query);
                $result = $stmt->execute([$name, $desc, $price, $year, $pages]);

                if ($result) {

                    $sucMessage .= "\\n Knjiga je uspesno upisana u bazu";
                  
                    $id_book = $conn -> query("select id as last from knjiga where id_slika = $id_pic") -> fetch() -> last;

                    foreach ($genre as $g) {

                        $res = $conn -> query("INSERT INTO knjiga_zanr VALUES(null, $id_book, $g)");
                        if($res) {
                            $sucMessage .= " \\n Uspesno upisan zanr knjige";
                        } 
        
                    }

                    foreach ($author as $a) {

                        $res = $conn -> query("INSERT INTO knjiga_autor VALUES(null, $id_book, $a)");

                        if($res) {
                            $sucMessage .= " \\n Uspesno upisan  autor  knjige";
                        } 
              
                    }
                   

                    $conn->commit();

                    $_SESSION['sucAdd'] = $sucMessage;
                    header("Location: ../../admin/" . $what);
                    die();
                   

                } 

            } 

        } catch (PDOException $ex) {

            $conn->rollback();

            if(file_exists($newPath)){
                unlink($newPath);
            }

            $exMessage = $ex -> getMessage();

            $erMessage =  " Greska pri upisu knjige u bazu \\n ";                     

            if(strpos($exMessage, "1062") !== false) {
                $erMessage .= " \\n Već postoji knjiga sa tom kombinacijom naziva, izdavača i godine izdanja. ";
            } else {
                $erMessage .= " \\n Greska: " . $exMessage;
            }

           

        }

    } else {
        $erMessage = "Neuspeh pri upload-ovanju slike na server";

    }

} else {

    $erMessage = "Slika ne zadovoljava kriterijume: ";

    foreach ($errs as $err) {
        $erMessage .= "\\n{$err}";
    }

}

if (isset($erMessage)) {

    zabeleziGresku($erMessage);

    $_SESSION['errAdd'] = $erMessage . "\\n Zbog nastanka greske, slika je obrisana sa servera i nista nije upisano u bazu, pokusajte ponovo";

    header("Location: ../../admin/" . $what);
    die();
}

?>









