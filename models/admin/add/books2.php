<?php

$alt = $_POST['tbAlt'];
$imgName = $_POST['tbImgName'];
$file = $_FILES['fImg'];
$id_pic = 0;

$errs = checkUploadedImage($file);

if (count($errs) == 0) {

    $sucMessage = " Slika ispunjava kriterijume";

    $tmpPath = $file['tmp_name'];
    $fileName = time() . "_" . $imgName . "." . pathinfo($file['name'], PATHINFO_EXTENSION);
    $newPath = "../../assets/images/books/" . $fileName;
    $moveSucc = move_uploaded_file($tmpPath, $newPath);

    if ($moveSucc) {

        $sucMessage .= "\\n Slika je upload-ovana na server";

        $query = "INSERT INTO slika VALUES (null, ?, ?)";

        $result = executePreparedStatementQuery($query, [$newPath, $alt]);

        if ($result['uspeh']) {

            $sucMessage .= "\\n Putanja do slike i ostale informacije su upisane u bazu";

            $id_pic = executeQueryOneRow("select id as last from slika where src = '$newPath'")->last;

            $name = $_POST['tbName'];
            $desc = $_POST['taDesc'];
            $price = $_POST['tbPrice'];
            $year = $_POST['tbYear'];
            $pages = $_POST['tbPages'];
            $publisher = $_POST['ddlPublisher'];
            $genre = $_POST['ddlGenre'];
            $author = $_POST['ddlAuthor'];

            $query = "INSERT INTO knjiga VALUES(null, ?, ?, ?, ?, ?, $publisher, $id_pic) ";

            $result = executePreparedStatementQuery($query, [$name, $desc, $price, $year, $pages]);

            if ($result['uspeh']) {

                $sucMessage .= "\\n Knjiga je uspesno upisana u bazu";

                $id_book = executeQueryOneRow("select id as last from knjiga where id_slika = $id_pic")->last;

                foreach ($genre as $g) {

                    if (executeQueryZeroRows("INSERT INTO knjiga_zanr VALUES(null, $id_book, $g)")) {
                        $sucMessage .= " \\n Uspesno upisan zanr knjige";
                    } else {
                        $erMessage =  " \\n Doslo je do problema prilikom upisa žanra knjige";
                    }

                }

                foreach ($author as $a) {

                    if (executeQueryZeroRows("INSERT INTO knjiga_autor VALUES(null, $id_book, $a)")) {
                        $sucMessage .= " \\n Uspesno upisan  autor  knjige";
                    } else {
                        $erMessage =  " \\n Doslo je do problema prilikom upisa autora knjige";
                    }

                }

                if (!isset($erMessage)) {
                    $_SESSION['sucAdd'] = $sucMessage;
                    header("Location: ../../admin/" . $what);
                }

            } else {
                $erMessage = "Greska pri upisu knjige u bazu \\n" . $result['greska'];
            }

        } else {
            $erMessage = "Greska pri unosu podataka o slici u bazu \\n" . $result['greska'];
        }

    } else {
        $erMessage = "Neuspeh pri upload-ovanju slike na server";

    }

} else {

    $erMessage = "Slika ne zadovoljava kriterijume: ";

    foreach ($errs as $err) {
        $erMessage .= "\\n{$err}";
    }

}

if (isset($erMessage)) {

    $_SESSION['errAdd'] = isset($sucMessage) ? "$sucMessage \\n\n" : "" . $erMessage . "\\n\\n Zbog nastanka greske, slika je obrisana sa servera i nista nije upisano u bazu, pokusajte ponovo";
    header("Location: ../../admin/" . $what);
}

?>




<?php

// $alt = $_POST['tbAlt'];
// $imgName = $_POST['tbImgName'];
// $file= $_FILES['fImg'];
// $id_pic = 0;

// $errs = checkUploadedImage($file);

// if(count($errs)==0){

//     $sucMessage = " Slika ispunjava kriterijume";

//     $tmpPath = $file['tmp_name'];
//     $fileName = time()."_".$imgName.".".pathinfo($file['name'], PATHINFO_EXTENSION);
//     $newPath = "../../assets/images/books/".$fileName;
//     $moveSucc = move_uploaded_file($tmpPath, $newPath);

//     if($moveSucc){

//         $sucMessage .= "\\n Slika je upload-ovana na server";

//         $query = "INSERT INTO slika VALUES (null, ?, ?)";

//         $result = executePreparedStatementQuery($query, [$newPath, $alt]);

//         // $insert = "INSERT INTO slika VALUES (null, :src, :alt)";
//         // $stmt = $conn->prepare($insert);
//         // $stmt->bindParam(":src", $newPath);
//         // $stmt->bindParam(":alt", $alt);
//         // $result = $stmt->execute();

//         if($result['uspeh']){

//            $sucMessage .= "\\n Putanja do slike i ostale informacije su upisane u bazu";

//            $id_pic = executeQueryOneRow("select id as last from slika where src = '$newPath'")->last;

//            $name = $_POST['tbName'];
//            $desc = $_POST['taDesc'];
//            $price = $_POST['tbPrice'];
//            $year = $_POST['tbYear'];
//            $pages = $_POST['tbPages'];
//            $publisher = $_POST['ddlPublisher'];
//            $genre = $_POST['ddlGenre'];
//            $author = $_POST['ddlAuthor'];

//            $query = "INSERT INTO knjiga VALUES(null, ?, ?, ?, ?, ?, $publisher, $id_pic) ";

//         //    $query = "INSERT INTO knjiga VALUES(null, :name, :desc, :price, :year, :pages,  $publisher, $id_pic) ";

//             $result = executePreparedStatementQuery($query, [$name, $desc, $price, $year, $pages]);

// //           $stmt = $conn->prepare($query);

//         //    $stmt->bindParam(":name", $name);
//         //    $stmt->bindParam(":desc", $desc);
//         //    $stmt->bindParam(":price", $price);
//         //    $stmt->bindParam(":year", $year);
//         //    $stmt->bindParam(":pages", $pages);

//         //    try{

//               // $success = $stmt->execute();

//                if($result) {

//                     $sucMessage .= "\\n Knjiga je uspesno upisana u bazu";

//                    $id_book = executeQueryOneRow("select id as last from knjiga where id_slika = $id_pic")->last;

//                     // try {
//                         foreach($genre as $g) {

//                             if(executeQueryZeroRows("INSERT INTO knjiga_zanr VALUES(null, $id_book, $g)")) {
//                                 $sucMessage .= " \\n Uspesno upisan zanr knjige";
//                             }

//                         }

//                         foreach($author as $a) {

//                              if(executeQueryZeroRows("INSERT INTO knjiga_autor VALUES(null, $id_book, $a)")) {
//                                  $sucMessage .= " \\n Uspesno upisan  autor  knjige";
//                              }

//                         }
//                     // } catch (PDOException $ex) {
//                     //     $erMessage = $ex -> getMessage() . " \\n Doslo je do problema prilikom upisa autora/zanra knjige";

//                     //     if(file_exists($newPath)){
//                     //         unlink($newPath);
//                     //     }

//                     // }

//                     if(!isset($erMessage)) {
//                         $_SESSION['sucAdd'] = $sucMessage;
//                         header("Location: ../../admin/" . $what);
//                     }

//                } else {

//                    $erMessage = "Interna greska servera";
//                }

//         //    } catch (PDOException $e) {

//         //         $erMessage = $e -> getMessage() . " \\n Vec postoji knjiga sa tom kombinacijom naziva, godine izdanja i izdavaca";

//         //         if(file_exists($newPath)){
//         //             unlink($newPath);
//         //         }

//         //    }

//         } else {
//             $erMessage = "Greska pri unosu podataka o slici u bazu \\n" . $result['greska'];
//         }

//     } else {
//         $erMessage = "Neuspeh pri upload-ovanju slike na server";

//     }

// } else {

//     $erMessage = "Slika ne zadovoljava kriterijume: ";

//     foreach($errs as $err){
//         $erMessage .= "\\n{$err}";
//     }

// }

// if(isset($erMessage)) {

//     $_SESSION['errAdd'] = isset($sucMessage) ? "$sucMessage \\n\n" : ""  .  $erMessage . "\\n\\n Zbog nastanka greske, slika je obrisana sa servera i nista nije upisano u bazu, pokusajte ponovo" ;
//     header("Location: ../../admin/" . $what);
// }

?>











