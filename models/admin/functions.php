<?php
    $tableArr = [
        "authors" => ["manual" => ["add" => "Dodajte novog autora, kombinacija imena i prezimena mora biti jedinstvena ", "update" => "Ovde menjate ime/prezime autora, vodite računa o jedinstvenoj kombinaciji ", "all" => "Prikaz svih autora uz broj knjiga. Oprezno sa brisanjem, ukoliko je jedini autor neke knjige, i ona će se obrisati"], "query" => "SELECT a.id, CONCAT(a.ime, ' ', a.prezime) as autor, COUNT(ka.id_autor) FROM autor a left join knjiga_autor ka on a.id = ka.id_autor group by a.id order by 2", "columns" => ['Autor', 'Broj knjiga']],
        "books" => ["manual" => ["add" => "Knjiga mora imati jedinstvenu kombinaciju naziva, izdavača i godine izdanja. Sva polja su obavezna, a žanrova i autora može imati više ", "update" => " Izmena knjige, moguća je i promena slike, voditi računa o jedinstvenosti", "all" => "Prikaz svih knjiga i njihovih autora, oprezno sa brisanjem"], "query" => "  select k.id, k.naziv, GROUP_CONCAT(CONcat(a.ime, ' ', a.prezime) SEPARATOR ', ') as autor  , k.cena from knjiga k inner join knjiga_autor ka on k.id = ka.id_knjiga inner join autor a on ka.id_autor = a.id group by k.id order by 2", "columns" => ['Knjiga','Autor','Cena']],
        "genres" => ["manual" => ["add" => "Dodajte novi žanr, naziv mora biti jedinstven ", "update" => "Izmena postojećeg žanra, naziv mora biti jedinstven ", "all" => "Prikaz svih žanrova uz broj knjiga. Oprezno sa brisanjem, ukoliko je jedini žanr neke knjige, i ona će se obrisati"], "query" => "SELECT z.*, COUNT(kz.id_knjiga) FROM zanr z LEFT JOIN knjiga_zanr kz ON z.id = kz.id_zanr GROUP BY z.id ORDER BY z.naziv", "columns" => ['Zanr', 'Broj knjiga']],
        "publishers" => ["manual" => ["add" => "Dodajte novog izdavača, naziv mora biti jedinstven ", "update" => "Izmena postojećeg žanra, naziv mora biti jedinstven ", "all" => "Prikaz svih izdavača uz broj knjiga. Oprezno sa brisanjem, sve knjige izdavača će biti obrisane"], "query" => "select i.*, count(k.id_izdavac)  from izdavac i left join knjiga k on i.id = k.id_izdavac GROUP by i.id order by i.naziv", "columns" => ['Izdavac', 'Broj knjiga']],
        "users" => ["manual" => ["add" => "Dodavanje novog korisnika, email adresa mora biti jedinstvena ", "update" => "Izmena postojećeg korisnika, email adresa mora biti jedinstvena, moguća i promena lozinke ", "all" => "Prikaz svih korisnika uz njihovu ulogu, oprezno sa brisanjem"], "query" => "select k.id ,CONCAT(k.ime, ' ', k.prezime) as ime_prezime, k.email, u.naziv  from korisnik k, uloga u where k.id_uloga = u.id order by 4, 2", "columns" => ['Ime i prezime', 'Email','Uloga']]
    ];



    function retrieveGetParam($param) {
        return isset($_GET[$param]) ? $_GET[$param] : null;
    }

    function getTableManual($table, $action) {
        global $tableArr;

        return $action ?  $tableArr[$table]['manual'][$action] : $tableArr[$table]['manual']['all'] ;
    }

    function populateTable($table) {
        global $tableArr;          

        $trs = executeQuery($tableArr[$table]['query']);        
        $ths = $tableArr[$table]['columns'];
        
        require_once "views/partials/admin/print_table.php";
        
        return $html;
        
    }

    function addOrUpdateTableResults($res, $what, $mesSucc, $mesErrEx) {
        if($res['uspeh']) {
            $_SESSION['sucAdd'] = $mesSucc ;
            header("Location: ../../admin/" . $what);
        } else if ($res['tipGreske'] == 'interna') {
            $_SESSION['errAdd'] = $res['greska'];
            header("Location: ../../admin/" . $what);
        } else {
            $greska = $res['greska'];
            if($res['kodIzuzetka'] == 1062) {
                $greska .= "\\n" . $mesErrEx; 
            }
            $_SESSION['errAdd'] = $greska;
            header("Location: ../../admin/" . $what);
        }
    }

    function checkUploadedImage($file) {

        $fileType = $file['type'];
        $fileSize = $file['size'];
        $allFormats = ["image/jpg", "image/jpeg", "image/png", "image/gif"];  
        $errs = [];
        if(!in_array($fileType,$allFormats)){
            array_push($errs, "Tip fajla nije validan. Dozvoljeni: jpg, jpeg, png, gif.");
        }    
        if($fileSize > 3000000){
            array_push($errs, "Velicina fajla ne sme biti veca od 3MB!");
        }
        return $errs;
    }


    function deleteFromTable($table, $id) { 
        global $conn;
        $stmt = $conn -> prepare("DELETE FROM $table WHERE id = $id");
        return $stmt -> execute();        
    }

    function deleteImg($id_book) {
        $row = executeQueryOneRow("SELECT s.id, src FROM slika s INNER JOIN knjiga k on s.id = k.id_slika WHERE k.id = $id_book") ;
        $src = $row -> src;
        $id = $row -> id;

        if(file_exists($src)){
            unlink($src);
        }
        deleteFromTable("slika", $id);
    }

    function deleteBookIfLeftWithoutGenreOrAuthor($table, $id) {
        
        $booksAffected;
        $booksAffected = executeQuery("SELECT id_knjiga, COUNT(*) as num  FROM knjiga_$table WHERE id_knjiga IN  ( SELECT id_knjiga FROM knjiga_$table WHERE id_$table = $id) group by id_knjiga ");

        if($booksAffected) {
            foreach($booksAffected as $bookAffected) {
                if($bookAffected -> num == 1 ) {               
                    $id_book = $bookAffected -> id_knjiga;
                    deleteImg($id_book);                    
                }
            }
        }
    }

    function deleteBookIfLeftWithoutPublisher ($id) {
        $booksAffected;
        $booksAffected = executeQuery("SELECT id FROM knjiga WHERE id_izdavac = $id");
        if($booksAffected) {
            foreach($booksAffected as $book) {                
                deleteImg($book -> id);
            }
        }
    }








?>