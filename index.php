<?php
session_start();

require_once "config/connection.php";
require_once "models/functions.php";

if (isset($_SESSION['forbidden'])) {
    echo "<script>alert(\"" . $_SESSION['forbidden'] . "\")</script>";
    unset($_SESSION['forbidden']);
    
}

 //var_dump($_GET);

if(count($_GET)) {
    if (isset($_GET['page'])) {
        $page = $_GET['page'];    
        //  var_dump ($page);    
        switch ($page) {
            case 'admin':case 'account':case 'author':case 'cart':case 'contact':case 'user':case '404':case '403': break;
            // default:$page = "main"; break;
            default:$page = "404"; break;
        }
    } else {
        $page = "404";
    }

} else {
    $page = "main";
}


if ($page == "admin" || $page == "user") {
    $roles = ["admin"=>"admin", "user" => "korisnik"];
    ifNotRoleForbid($roles[$page]);
}


$page_metadata = getPageMetadata($page);

require_once "views/fixed/head.php";
require_once "views/fixed/header.php";
require_once "views/pages/{$page}.php";
require_once "views/fixed/footer.php";

?>