<?php

require_once "config.php";

zabeleziPristupStranici();

try {
    $conn = new PDO("mysql:host=".SERVER.";dbname=".DATABASE.";charset=utf8", USERNAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
    echo $ex->getMessage();
    zabeleziGresku($ex -> getMessage());
}

function executeQuery($query){
    global $conn;
    return $conn->query($query)->fetchAll();
}

function executeQueryOneRow($query){
    global $conn;
    return $conn->query($query)->fetch();
}

function executePreparedStatementQuery($query, $paramsArr) {
    global $conn;
    $stmt = $conn -> prepare($query);
    
    try {
        $success = $stmt->execute($paramsArr);
        if($success) {
            $odgovor = ['uspeh' => true];
        } else {
            $odgovor = ['uspeh' => false, 'tipGreske' => "interna", "greska" => "Došlo je do interne greške servera, molimo pukušajte kasnije"];
        }
    }  catch(PDOException $ex) {
        $exMessage = $ex -> getMessage();
        if(strpos($exMessage, "1062") !== false) {
            $kodIzuzetka = 1062;
        } else {
            $kodIzuzetka = -1;
        }
        $odgovor = ['uspeh' => false, 'tipGreske' => 'izuzetak', 'greska' => "Žao nam je, došlo je do greške: \\n" . $exMessage, "kodIzuzetka" => $kodIzuzetka];
    }

    return $odgovor;

}


function zabeleziPristupStranici(){
    $open = fopen(LOG_FAJL, "a");
    if($open){
        $date = date('Y-m-d H:i:s');
        fwrite($open, "{$_SERVER['PHP_SELF']}\t{$date}\t{$_SERVER['REMOTE_ADDR']}\t\n");
        fclose($open);
    }
}

function zabeleziGresku($greska){
    $open = fopen(ERROR_FAJL, "a");
    if($open){
        $date = date('Y-m-d H:i:s');
        fwrite($open, "{$_SERVER['PHP_SELF']}\t{$date}\t{$greska}\t\n");
        fclose($open);
    }
}