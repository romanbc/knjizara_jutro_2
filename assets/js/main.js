/* eslint-disable no-console */
/* eslint-disable no-undef */
$(document).ready(function () {
    // AOS.init({
    //     duration: 700,
    //     easing: "ease-in-out",
    //     once: true
    // });

    ajaxGetBooks();

    // $(".buy").on("click", eventBuy);
    $("body").on("click", ".buy", eventBuy);
    $("#bookDetails").on("show.bs.modal", eventDetails);
    $("#resetFilters").on("click", function () {
        removeLS("genreId");
        removeLS("publisherIds");
        removeLS("sortBy");
        window.location = "./";
    });

    ajaxGetGenres();
    ajaxGetPublishers();

    $("#formFS").on("submit", function (event) {
        event.preventDefault();

        let sortBy = $("select[name=ddlSort]").val();
        // console.log(sortBy);
        // console.log(sortBy.split("-"));


        let genreId = $("select[name=ddlGenre]").val();


        let publisherIds = [];

        $(".publisher:checked").each(function () {
            publisherIds.push($(this).val());
        });

        if (!publisherIds.length) {
            publisherIds = 0;
        } else {
            publisherIds = publisherIds.join(",");
        }

        // console.log(publisherIds);

        setLS("publisherIds", publisherIds);

        if (hasLS("genreId") && getLS("genreId") != genreId) {
            removeLS("publisherIds", publisherIds);
        }

        setLS("sortBy",sortBy);

        setLS("genreId", genreId);

        // ajaxGetBooks(0, genreId, publisherIds );
        ajaxGetBooks();

        ajaxGetPublishers();

    });

    $("body").on("click", ".books-pagination", function () {
        let limit = $(this).data("limit");
        ajaxGetBooks(limit);

    });
});


function ajaxGetPublishers() {

    let genreId = hasLS("genreId") ? getLS("genreId") : 0;


    // console.log(genreId);

    // console.log(hasLS("genreId"));


    $.ajax({
        url: "models/books/get_all_publishers.php",
        method: "GET",
        data: {
            genreId
        },
        success: function (data) {
            console.log(data);
            printPublishers(data);

        },
        error: function (error) {
            console.log(error);
        }
    });
}

function ajaxGetGenres() {
    $.ajax({
        url: "models/books/get_all_genres.php",
        method: "GET",
        success: function (data) {
            console.log(data);
            printGenres(data);

        },
        error: function (error) {
            console.log(error);
        }
    });
}

function printPublishers(publishers) {

    publisherIds = [0];
    if (hasLS("publisherIds") && getLS("publisherIds") != 0) {

        publisherIds = getLS("publisherIds").split(",");
        console.log(publisherIds);
    }


    html = "";
    // <?php  if($publisherIds) echo  (in_array($p->id, $publisherIds) ? " checked" : "" ); ?>
    for (let publisher of publishers) {
        html +=
            `
        <div class="form-check">
        <input class="form-check-input  publisher" type="checkbox"
        name="chbPublishers[]" value="${publisher.id}" id="publisher-${publisher.id}"              
            ${ publisherIds.includes(publisher.id) ? " checked " : ""}    />
        <label class="form-check-label d-flex" for="publisher-${publisher.id}">
        ${publisher.naziv}
        <span class="ml-auto"> (${publisher.broj})</span>
        </label>
        </div>
        `;
    }
    $("#publishers").html(html);
}

function printGenres(genres) {

    genreId = getLS("genreId");
    html = "<option value=\"0\">SVI</option>";
    // ${ genreId == genre.id ? "selected" : ""  }
    for (let genre of genres) {
        html += `<option value="${genre.id}"  ${genreId == genre.id ? "selected" : ""}
        >${genre.naziv}</option>`;
    }
    $("select[name=ddlGenre]").html(html);
}

function ajaxGetBooks(limit = 0) { //, genreId = 0, publisherIds) {

    let genreId = hasLS("genreId") ? getLS("genreId") : 0;
    let publisherIds = hasLS("publisherIds") ? getLS("publisherIds") : 0;
    let sortBy = hasLS("sortBy") ? getLS("sortBy") : 0 ;

    $.ajax({
        url: "models/books/get_all_with_pagination.php",
        method: "GET",
        data: {
            limit, genreId, publisherIds, sortBy
        },
        success: function (data) {
            // console.log(data);

            //printMovies(data.movies);
            printBooks(data.books);
            printPagination(data.num_of_pages);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function printBooks(books) {
    let html = "";
    if(books.length) {
        for (let book of books) {
            html += printBook(book);
        }

    } else {
        html += `<h5 class="text-info">Ne postoje knjige koje zadovoljavaju te kriterijume</h5>`;
    }

    $("#books").html(html);
    // AOS.refresh();

}

function printBook(book) {

    return `<div class="book mb-2 col-md-4 col-sm-6" data-aos="fade-up">
        <div class="card border mx-md-0 mx-lg-1">
            <img class="card-img-top lazyload" data-src="${book.src.substring(4)}" alt="${book.alt}">
            <div class="card-body pt-0 text-center">
                <h5 class="card-title text-primary text-uppercase">${book.naziv}</h5>
                <h6 class="card-subtitle text-success">${book.autor}</h6>
                <p class="my-1 text-danger">${book.cena} RSD</p>
                <button type="button" class="btn btn-outline-primary details" data-toggle="modal"
                    data-target="#bookDetails" data-id="${book.id}">Detalji</button>
                <a href="#" data-id="${book.id}" class="buy mx-1 btn btn-outline-success">Kupi</a>
            </div>
        </div>
    </div>`;
}


function printPagination(numOfPages) {
    let html = "";
    if (numOfPages > 1) {
        for (let i = 0; i < numOfPages; i++) {
            html += `<li>
            <a href="#" class="books-pagination mx-2" data-limit="${ i}">
            ${ i + 1}
            </a>
            </li>`;
        }
    }
    $("#pagination").html(html);
}






function eventDetails(e) {
    let bookId;
    bookId = $(e.relatedTarget).data("id");
    let thiss = $(this);

    $.ajax({
        // url: "views/main/bookDetails.php?id=" + bookId,
        url: "models/books/get_ones_details.php?id=" + bookId,
        method: "GET",
        success: function (data) {
            console.log(data);
            $(thiss).find(".modal-body").html(printBookDetails(data));
            $(".buy-details").on("click", eventBuyDetails);
        },
        error: function (xhr) {
            // eslint-disable-next-line no-console
            console.error(xhr.status);
        }
    });
}

function printBookDetails(book) {
    return `<div class="container">
    <div class="row">
        <div class="col-md-7 mb-0">
            <h5 class="text-uppercase text-primary">
            
                ${ book.naziv} 
            
            </h5>
            <h6>
                ${ book.zanr}
            </h6>
            <h6 class="text-success">
            
                ${ book.autor} 
            </h6>
            <p class="border-bottom mb-0 ">
            
                ${ book.opis} 
            
            
            
            </p>
            <div class="p-1 d-flex justify-content-between align-items-center">
                <div class="text-danger">
                
                ${ book.cena} 
                
                
                </div>
                <div>
                <a href="#" data-id="
                    ${ book.id} 
                " 
                
                class="buy-details mx-1 btn btn-outline-success">Kupi</a>
                </div>
            </div>
        </div>
        <div class="col-md-5 order-md-first mt-0">
            <div>
                <ul class="list-group list-group-flush mb-2">
                    <li class="list-group-item p-1">Izdavač: 
                    ${ book.izdavac} 
                    
                    </li>
                    <li class="list-group-item p-1">Broj strana: 
                    
                    ${ book.strana} 

                    </li>
                    <li class="list-group-item p-1">Godina izdanja: 
                    
                    ${ book.godina_izdanja} 

                    </li>
                </ul>
            </div>
            <img class="img-fluid lazyload" 
      
            data-src="${ book.src.substring(4)} " 

            alt="${ book.alt} ">
             
             
          
        </div>
    </div>
</div>`;
}


function eventBuy(e) {
    e.preventDefault();
    let id = $(this).data("id");
    addToCart(id);
    toTop();
}

function eventBuyDetails(e) {
    e.preventDefault();
    let id = $(this).data("id");
    $("#bookDetails").modal("hide");
    addToCart(id);
    toTop();
}

function addToCart(id) {
    let books = getLS("books");
    if (books) {
        if (bookIsAlreadyInCart()) {
            updateQuantity();
        } else {
            addBookToLocalStorage();
        }
    } else {
        addFirstBookToLocalStorage();
    }

    printCartCount();

    function bookIsAlreadyInCart() {
        return books.filter(book => book.id == id).length;
    }

    function updateQuantity() {
        let books = getLS("books");
        for (let book in books) {
            if (books[book].id == id) {
                books[book].quantity++;
                break;
            }
        }
        setLS("books", books);
    }

    function addBookToLocalStorage() {
        let books = getLS("books");
        books.push({
            id: id,
            quantity: 1
        });
        setLS("books", books);
    }

    function addFirstBookToLocalStorage() {
        let books = [];
        books[0] = {
            id: id,
            quantity: 1
        };
        setLS("books", books);
    }
}

