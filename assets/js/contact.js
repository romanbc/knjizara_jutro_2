/* eslint-disable no-unused-vars */

$(document).ready(function() {
    $("form input:text, form textarea").on("blur", eventBlurField);
    $("form").on("submit", eventSubmitForm);
});

let valid, regExps;
regExps = {
    regMail: /^\w+([.-]?[\w\d]+)*@\w+([.-]?[\w]+)*(\.\w{2,4})+$/,
    regBody: /^[\sA-zČĆŽŠĐčćžšđ_.-\d?,:;()„”]{5,1000}$/,
    regName: /^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14}(\s[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14}){1,4}$/
};

function eventBlurField() {
    let field = $(this);
    if (field.val().trim() === "") {
        field.removeClass("is-valid is-invalid");
    } else {
        singleFieldCheck(field, findRegExOfField(field));
    }
}

function eventSubmitForm(event) {
    //event.preventDefault();
    valid = true;
    let fields = $(this).find("input:text, textarea");
    for (let field of fields) {
        singleFieldCheck(field, findRegExOfField(field));
    }
    if (!valid)  {
        event.preventDefault();
    }
}

function singleFieldCheck(field, regularExp) {
    if (
        !regExps[regularExp].test(
            $(field)
                .val()
                .trim()
        )
    ) {
        valid = false;
        $(field).addClass("is-invalid");
    } else {
        $(field)
            .removeClass("is-invalid")
            .addClass("is-valid");
    }
}

function findRegExOfField(field) {
    return (
        "reg" +
        $(field)
            .prop("name")
            .substring(2)
    );
}
