/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

$(document).ready(function() {
    $("form input:text, form input:password, form textarea").on("blur", eventBlurField);
    $("form").on("reset", eventResetForm);
    $("form").on("submit", eventSubmitForm);
});

let valid, regExps;
regExps = {
    regMail: /^\w+([.-]?[\w\d]+)*@\w+([.-]?[\w]+)*(\.\w{2,4})+$/,
    regPass: /^(?=.*\d)(?=.*[A-zČĆŽŠĐčćžšđ])(?=.*[~!@#$%^&*<>?]).{6,}$/,
    regPersonName: /^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14}(\s[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14}){0,5}$/,
    regName100: /^[\sA-zČĆŽŠĐčćžšđ_.-\d]{2,100}$/,
    regPrice: /^[1-9]\d{0,7}(\.\d{1,2})?$/,
    regDescTa: /^[\sA-zČĆŽŠĐčćžšđ_.-\d?,:;()„”]{5,1000}$/,
    regImgName: /^[A-z\d_]{2,150}$/
};

function eventBlurField() {
    let field = $(this);
    if (field.val().trim() === "") {
        field.removeClass("is-valid is-invalid");
    } else {
        singleFieldCheck(field, findRegExOfField(field));
    }
}

function eventResetForm() {
    $(this)
        .find("input:text, input:password")
        .removeClass("is-valid is-invalid");
}

function eventSubmitForm(event) {
    valid = true;
    let fields = $(this).find("input:text, input:password, textarea");
    for (let field of fields) {
        singleFieldCheck(field, findRegExOfField(field));
    }
    if (!valid) {
        event.preventDefault();
    } 
   
}

function singleFieldCheck(field, regularExp) {
    if (
        !regExps[regularExp].test(
            $(field).val().trim()
        )
    ) {
        valid = false;
        $(field).addClass("is-invalid");
    } else {
        $(field)
            .removeClass("is-invalid")
            .addClass("is-valid");
    }
}

function findRegExOfField(field) { 
    if($(field).hasClass("person-name")) {
        return "regPersonName";
    } else if ($(field).hasClass("name-100")) {
        return "regName100";
    } else if($(field).prop("name") == "tbPrice") {
        return "regPrice";
    } else if($(field).prop("name") == "taDesc"){
        return "regDescTa";
    } else if ($(field).prop("name") == "tbImgName") {
        return "regImgName";
    } else {   
        return ("reg" + $(field).prop("name").substring(2));
    }
}


