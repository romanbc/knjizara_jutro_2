/* eslint-disable no-console */
/* eslint-disable no-undef */
$(document).ready(function() {
    if (!hasLS("books")) {
        showEmptyCart();
    } else {
        booksCart = getLS("books");
        booksCartIds = booksCart.map(bc => bc.id);

        getBooksFromAjax();
    }
});

let booksCart, booksAjax, booksAjaxCart;

let booksCartIds;


function getBooksFromAjax() {
    $.ajax({
        url: "models/books/get_all_from_cart.php",
        method: "POST",
        data: {
            ids:  booksCartIds,
            safe: "y"
        },
        success: function(data) {
            // console.log(data);

         
            booksAjax = [...data];
            setBooksAjaxCart();
            generateTable();
            // console.log(data);
        }, 
        error: function(xhr) {
            console.error(xhr);
        }
    });


}

function setBooksAjaxCart() {
    booksAjaxCart = booksAjax.filter(bookA => {
        for (let bookLS of booksCart) {
            if (bookA.id == bookLS.id) {
                bookA.quantity = bookLS.quantity;
                return true;
            }
        }
        return false;
    });
}

function generateTable() {
    let html = `
        <table class="table table-responsive-lg text-center">
            <thead class="text-info">
                <tr>
                    <th></th>
                    <th>Knjiga</th>
                    <th>Cena</th>
                    <th>Količina</th>
                    <th>Ukupno</th>
                    <th>Ukloni</th>
                </tr>
            </thead>
        <tbody>
    `;
    for (let bookAjaxLS of booksAjaxCart) {
        html += generateTr(bookAjaxLS);
    }
    html += `</tbody></table>`;
    $("#cart").html(html);
    $(".remove").on("click", removeBookFromCart);
    $(".minus-one").on("click", minusOneBook);
    $(".plus-one").on("click", plusOneBook);
    totalPrice();
    $("#remove-all").on("click", removeAllBooksFromCart);
}

function generateTr(book) {
    return `
        <tr>
            <td class="align-middle">
                <img
                    alt="${book.alt}"
                    src="${book.src.substring(4)}"
                    width="60em"
                />
            </td>
            <td class="align-middle text-primary">
                ${book.naziv}
            </td>
            <td class="align-middle">${book.cena} RSD</td>
            <td class="align-middle" style="white-space: nowrap;">
                <button type="button" data-id="${
                    book.id
                }" class="minus-one btn btn-sm btn-outline-danger " >-</button>
                ${book.quantity} 
                <button type="button" data-id="${
                    book.id
                }" class="plus-one btn btn-sm btn-outline-success" >+</button>
            </td>
            <td class="align-middle">${book.cena *
                book.quantity} RSD</td>
            <td class="align-middle text-danger">
                <button type="button" aria-label="ukloni iz korpe" data-id="${
                    book.id
                }" class="btn btn-sm btn-outline-danger remove">
                    <i class="icon-remove"></i>
                </button>
            </td>
        </tr>
    `;
}



// function showEmptyCart() {
//     $("#cart").html(
//         '<h1 class="text-center my-3 text-info">Vaša korpa je trenutno prazna. Pogledajte našu ponudu <a href="index.php"> knjiga.</a></h1>'
//     );
// }

function totalPrice() {
    let total = 0;
    for (let bookAjaxLS of booksAjaxCart) {
        for (let bookLS of booksCart) {
            if (bookAjaxLS.id == bookLS.id) {
                total +=
                    bookAjaxLS.cena * bookAjaxLS.quantity;
            }
        }
    }
    $("#cart").append(
        `<div class="row lead font-weight-bold text-center">
            <p class="col-sm-3 text-info">Ukupno: ${total} RSD</p>
            <p class="col-sm-3"><a href="#" id="remove-all" class="text-danger">Ukloni sve</a></p>
            <p class="col-sm-3"><a href="./">Dodaj knjige</a></p>
            <p class="col-sm-3"><a href="account">Završi kupovinu</a></p>
        </div>`
    );
}

function removeBookFromCart() {
    let id = $(this).data("id");
    booksCart = getLS("books");
    let booksFiltered = booksCart.filter(bookCart => bookCart.id != id);
    setLS("books", booksFiltered);
    booksCart = getLS("books");
    if (booksCart.length) {
        setBooksAjaxCart();
        generateTable();
    } else {
        removeLS("books");
        showEmptyCart();
    }
    printCartCount();
}

function removeAllBooksFromCart(e) {
    e.preventDefault();
    removeLS("books");
    showEmptyCart();
    printCartCount();
}

function minusOneBook() {
    let id = $(this).data("id");
    booksCart = getLS("books");
    let booksFiltered = [];

    booksCart.forEach(bookCart => {
        if (bookCart.id == id) {
            if (--bookCart.quantity > 0) {
                booksFiltered.push(bookCart);
            }
        } else {
            booksFiltered.push(bookCart);
        }
    });
    setLS("books", booksFiltered);

    booksCart = getLS("books");
    if (booksCart.length) {
        setBooksAjaxCart();
        generateTable();
    } else {
        removeLS("books");
        showEmptyCart();
    }
    printCartCount();
}

function plusOneBook() {
    let id = $(this).data("id");
    let books = getLS("books");
    for (let book of books) {
        if (book.id == id) {
            book.quantity++;
            break;
        }
    }
    setLS("books", books);
    booksCart = getLS("books");
    setBooksAjaxCart();
    generateTable();
    printCartCount();
}
